<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use App\Services\GrantedService;

class AppController extends AbstractController
{
    #[Route('/', name: 'app_app', methods: ['GET', 'POST'])]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
         
           if ($this->isGranted('ROLE_ADMIN')) {
                return $this->redirectToRoute('admin');
           }

           if ($this->isGranted('ROLE_USER')) {
               $currentUser = $this->getUser();
                if ($this->getUser()->getStatus() == 0) {
                    return $this->redirectToRoute('users');
                }
                if ($this->getUser()->getStatus() == 1) {

                    return $this->render('profil/has_demand.html.twig', [
                        'current' => $currentUser,
                        'user'=> $currentUser->getParrain()
                    ]);
                }
                if ($this->getUser()->getStatus() == 2) {
                    return $this->render('profil/got_demand.html.twig', [
                        'current' => $currentUser,
                        'user'=> $currentUser->getParrain()]
                    );
                }
                if ($this->getUser()->getStatus() == 3) {
                    return $this->render('profil/has_friend.html.twig', [
                        'current' => $currentUser,
                        'user'=> $currentUser->getParrain()]
                    );
                }
       }
         
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('app/index.html.twig', [
            'controller_name' => 'AppController',
            'last_username' => $lastUsername,
            'error' => $error]
        );
    }

    /**
     * This controller allow us to logout
     *
     * @return void
     */
    #[Route('/deconnexion', 'app_logout')]
    public function logout()
    {
        // Nothing to do here..
    }

}
