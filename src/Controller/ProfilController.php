<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;
use App\Repository\UserRepository;
use App\Services\AcceptFriend;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class ProfilController extends AbstractController
{
    #[Route('/profil', name: 'app_profil')]
    public function index(): Response
    {
        return $this->render('profil/index.html.twig', [
            'controller_name' => 'ProfilController',
        ]);
    }

    #[Route('/user/{id}', name: 'user_profil')]
    public function profil(UserRepository $userRepository, int $id): Response
    {
        $currentUser = $this->getUser();
        $user = $userRepository
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No user found for id '.$id
            );
        }
        return $this->render('profil/index.html.twig', [
            'user' => $user,
            'current' => $currentUser 
        ]);
    }

    #[Route('/users', name: 'users')]
    public function profils(UserRepository $userRepository): Response
    {
        
        $currentUser = $this->getUser();
        $users = $userRepository
         ->findAllExceptThis($currentUser->getId());;

       
        return $this->render('profil/users.html.twig', [
            'users' => $users,
            'current' => $currentUser 
        ]);
    }

    #[Route('/add_parrain/{id}', name: 'add_parrain')]
    public function addParrain(AcceptFriend $ac, int $id): Response
    {
         $currentUser = $this->getUser();
         if ($ac->send($id, $currentUser) == 1) {
            
                 $this->addFlash('success', 'Votre demande à bien été envoyé');
             } else   {

                $this->addFlash('error', 'Oups cet utilisateurs a deja une demande en cours');
            }
        
            return $this->render('profil/has_demand.html.twig', [
                'current' => $currentUser,
                'user'=> $currentUser->getParrain()
            ]);
    }

    #[Route('/accept_demand', name: 'accept_demand')]
    public function acceptDemand(AcceptFriend $ac): Response
    {
         $currentUser = $this->getUser();

         
         if ($ac->accept($currentUser) == 1) {
            
                 $this->addFlash('success', 'Felicitation vous avez une relation');
             } else   {

                $this->addFlash('error', 'Oups cet utilisateurs a deja une demande en cours');
            }
        
            return $this->render('profil/has_friend.html.twig', [
                'current' => $currentUser,
                'user'=> $currentUser->getParrain()
            ]);
    }

    #[Route('/refuse_demand', name: 'refuse_demand')]
    public function refuseDemand(AcceptFriend $ac): Response
    {
         $currentUser = $this->getUser();

         
         if ($ac->refuse($currentUser) == 1) {
            
                 $this->addFlash('success', 'Votre demande est bien tenu en compte');
             } else   {

                $this->addFlash('error', 'Oups cet utilisateurs a deja une demande en cours');
            }
        
            return $this->redirectToRoute('users');
        }
    
}   
