<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\User;
use Faker\Generator;
;

class AppFixtures extends Fixture
{
    /**
     * @var Generator
     */
    private Generator $faker;

    public function __construct()
    {
        $this->faker = Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager): void
    {

        $users = [];
        
        $admin = new User();
        $admin
            ->SetEmail('admin@admin.com')
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setPlainPassword('admin1234');

        $users[] = $admin;
        $manager->persist($admin);


        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setNom($this->faker->firstname())
                ->setPreNom($this->faker->lastname())
                ->setDescription($this->faker->paragraph())
                ->setNiveau($this->faker->numberBetween(0, 5))
                ->setformation($this->faker->randomElement(['EPSI-Informatique-Reseau', 'EPSI-Informatique-Dev', 'ESIR-DEV']))
                ->setEmail($this->faker->email())
                ->setStatus(0)
                ->setRoles(['ROLE_USER'])
                ->setPlainPassword('password')
                ->setHasParrain(false);

            $users[] = $user;
            $manager->persist($user);
        }


        $manager->flush();
    }
}
