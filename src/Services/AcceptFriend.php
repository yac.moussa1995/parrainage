<?php
namespace App\Services;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AcceptFriend
{
    private $doctrine;
    private $userRepository;
    /**
     * Constructor
     *
     * @param UserRepository $userRepository
     * @param ManagerRegistry $doctrine
     */
    public function __construct(UserRepository $userRepository, ManagerRegistry $doctrine) {
        $this->doctrine = $doctrine;
        $this->userRepository = $userRepository;
    }
    
    public function send(int $id, User $currentUser) {
        $entityManager = $this->doctrine->getManager();
        if ($currentUser->getStatus() == 0) {
            $user = $this->userRepository
                    ->find($id);
            if ($user->getStatus() == 0) {   
                $user->setParrain($currentUser);
                $user->setStatus(2);
                $entityManager->persist($user);
                $entityManager->flush();

                $currentUser->setHasParrain(true);
                $currentUser->setStatus(1);
                $currentUser->setParrain($user);
                $entityManager->persist($currentUser);
                $entityManager->flush();
                return 1;
            } else   {

               return 0;
           }
        }
        else   {

           return 0;
       }
    }

    public function accept(User $currentUser) {
       
        $entityManager = $this->doctrine->getManager();
         if ($currentUser->getStatus()== 2) {
            $user = $currentUser->getParrain();
                if ($user->getStatus() == 1) {   
                    $user->setStatus(3);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    $currentUser->setStatus(3);
                    $entityManager->persist($currentUser);
                    $entityManager->flush();
                    return 1;
                } 

                return 0;
            }
            
    }

    public function refuse(User $currentUser) {
       
        $entityManager = $this->doctrine->getManager();
        if ($currentUser->getStatus()== 2) {
           $user = $currentUser->getParrain();
                   $user->setStatus(0);
                   $user->setParrain(null);
                   $entityManager->persist($user);
                   $entityManager->flush();

                   $currentUser->setStatus(0);
                   $currentUser->setParrain(null);
                   $entityManager->persist($currentUser);
                   $entityManager->flush();
                   return 1;
               
        } else   {

            return 0;
        }
    }
          
}